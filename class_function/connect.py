import pymysql.cursors
class Connect():
	def __init__(self,query='',qtype=''):
		self._query=query
		self._qtype=qtype
		self._connection = pymysql.connect(host='localhost',
                             user='root',
                             password='',
                             db='wallet',
                             charset='utf8',
                             cursorclass=pymysql.cursors.DictCursor)

	def set_query(self,query):
		self._query=query

	def get_query(self):
		return self._query

	def set_qtype(self,qtype):
		self._qtype=qtype

	def get_qtype(self):
		return self._qtype

	def get_query(self):
		with self._connection.cursor() as cursor:
			cursor.execute(self._query)
			if self._qtype=='one':
				result=cursor.fetchone()
			else:
				result=cursor.fetchall()

		return result

	def insert(self):
		with self._connection.cursor() as cursor:
			result=cursor.execute(self._query)
			lastid=cursor.lastrowid
		self._connection.commit()

		return lastid

	def update(self):
		with self._connection.cursor() as cursor:
			result=cursor.execute(self._query)
		self._connection.commit()

		return 'update complete'


		
		