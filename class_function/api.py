import urllib.request
import json
class API():
	def __init__(self,currency="thb"):
		self._currency=currency
		self._api_cur="https://api.fixer.io/latest"

	def get_currency(self):
		try:
			request = urllib.request.Request(self._api_cur+"?base="+self._currency)
			response = urllib.request.urlopen(request).read().decode('utf-8')
			jsondata = json.loads(response)
			return jsondata
		except Exception as e:
			return	{'error':'this currency is not Supported'}
		
