from class_function import connect
from class_function import api
import datetime

def dashboard():

	query=connect.Connect("SELECT * FROM user WHERE id = 1","one")
	user=query.get_query()

	menu=0
	print("Hi ",user['firstname'],"",user['lastname'],"")
	while(menu !=7 ):
		if(menu==0):
			print("Welcome to Your Wallet")
			print("Please Select Menu")
			print("1.) Views Exchange Rate ")
			print("2.) Deposit")
			print("3.) Withdraw")
			print("4.) Covert Currency")
			print("5.) Views Wallet")
			print("6.) Views Transactions")
			print("7.) Exit")
			menu=int(input('Enter Menu : '))
		elif(menu==1):
			print('Wallet > Views Exchange Rate')
			currency_exchange()
			menu=int(input('0.) Back to Dashboard : '))
			menu=0
		elif(menu==2):
			print('Wallet > Deposit')
			ins=deposit_money()
			if ins == 1:
				menu=0
			else:
				menu=int(input('0.) Back to Dashboard : '))
				menu=0
		elif(menu==3):
			print('Wallet > Withdraw')
			ins=withdraw_money()
			if ins == 1:
				menu=0
			else:
				menu=int(input('0.) Back to Dashboard : '))
				menu=0
		elif(menu==4):
			print('Wallet > Convert Currency')
			conv=convert_currency()
			if conv == 1:
				menu=0
			else:
				menu=int(input('0.) Back to Dashboard : '))
				menu=0
		elif(menu==5):
			print('Wallet > Views Wallet')
			views_wallet()
			menu=int(input('0.) Back to Dashboard : '))
			menu=0
		elif(menu==6):
			print('Wallet > Views Transactions')
			views_transaction()
			menu=int(input('0.) Back to Dashboard : '))
			menu=0
		else:
			menu=0

def deposit_money():
	try:
		currency=input('Enter Currency to deposit (Example : sgd,usd,thb,...) : ').lower()
		getapi=api.API(currency)
		jsondata=getapi.get_currency()
		if 'error' in jsondata:
			print(jsondata['error'])
		else:
			amtmoney=float(input(f'Enter amount of money ({currency}) to deposit : '))
			if amtmoney<=0:
				print('Please Enter money more than 0')
				return 0
			else:
				query=connect.Connect(f"SELECT * FROM user_account_relation uar INNER JOIN account a ON uar.account_id=a.id WHERE uar.user_id=1 AND a.account_name='{currency}'","one")
				chkacc=query.get_query()
				if(chkacc==None):
					query.set_query(f"INSERT INTO account SET account_name='{currency}'")
					account_id=query.insert()
					query.set_query(f"INSERT INTO user_account_relation SET user_id=1,account_id={account_id},amount={amtmoney}")
					add_relation=query.insert()
				else:
					account_id=chkacc['account_id']
					newmoney=float(chkacc['amount'])+amtmoney
					query.set_query(f"UPDATE user_account_relation SET amount='{newmoney}' WHERE user_id=1 AND account_id={account_id}")
					update=query.update()

				query.set_query(f"INSERT INTO transactions SET user_id=1,account_id={account_id},amt={amtmoney},action='deposit',created_at='{datetime.datetime.now()}'")
				add_transection=query.insert()

				print('Deposit Complete\n')
				return 1

	except Exception as e:
		print(e)


def withdraw_money():
	try:
		currency=input('Enter Currency to withdraw (Example : sgd,usd,thb,...) : ').lower()
		getapi=api.API(currency)
		jsondata=getapi.get_currency()
		if 'error' in jsondata:
			print(jsondata['error'])
		else:

			query=connect.Connect(f"SELECT * FROM user_account_relation uar INNER JOIN account a ON uar.account_id=a.id WHERE uar.user_id=1 AND a.account_name='{currency}'","one")
			chkacc=query.get_query()
			if(chkacc==None):
				print('You don\'t have this currency')

			else:
				curmoney=chkacc['amount']
				print(f'Your amount of money is {float(curmoney)} {currency}')
				amtmoney=float(input(f'Enter amount of money ({currency}) to withdraw : '))
				if amtmoney<=0:
					print('Please Enter money more than 0')
					return 0
				else:
					account_id=chkacc['account_id']
					if(amtmoney>curmoney):
						print('Your money is not enough')
					else:
						newmoney=float(curmoney)-float(amtmoney)
						query.set_query(f"UPDATE user_account_relation SET amount='{newmoney}' WHERE user_id=1 AND account_id={account_id}")
						update=query.update()
						query.set_query(f"INSERT INTO transactions SET user_id=1,account_id={account_id},amt={amtmoney},action='withdraw',created_at='{datetime.datetime.now()}'")
						add_transection=query.insert()
						print('Withdraw Complete\n')
						return 1

	except Exception as e:
		print(e)

def convert_currency():
	try:
		sel_currency=input('Enter Currency (Example : sgd,usd,thb,...) : ').lower()
		getapi=api.API(sel_currency)
		jsondata=getapi.get_currency()
		if 'error' in jsondata:
			print(jsondata['error'])
		else:
			query=connect.Connect(f"SELECT * FROM user_account_relation uar INNER JOIN account a ON uar.account_id=a.id WHERE uar.user_id=1 AND a.account_name='{sel_currency}'","one")
			chkacc=query.get_query()
			if(chkacc==None):
				print('You don\'t have this currency')
			else:	
				curmoney=float(chkacc['amount'])
				account_id=chkacc['account_id']
				print(f'Your amount of money is {curmoney} {sel_currency}')

				conv_currnecy=input('Enter currency to convert (Example : sgd,usd,thb,...) : ').lower()
				getapi_conv=api.API(conv_currnecy)
				jsondata_conv=getapi_conv.get_currency()
				if 'error' in jsondata_conv:
					print(jsondata_conv['error'])
				else:
					money=float(input(f'Enter amount of {sel_currency} (Your money is {curmoney} {sel_currency}) : '))
					if(money>curmoney):
						print('Your money is not enough')
						return 0
					elif(money<=0):
						print('Please Enter money more than 0')
						return 0
					else:
						rateconv=jsondata['rates'][conv_currnecy.upper()]
						amt_conv=convert_money(money,rateconv)
						print(f'Your will get {amt_conv} {conv_currnecy}')
						con_conv=input('Confirm ? (y/n)').lower()
						if(con_conv=='y'):

							newmoney=curmoney-money
							query.set_query(f"UPDATE user_account_relation SET amount='{newmoney}' WHERE user_id=1 AND account_id={account_id}")
							update=query.update()
							
							query.set_query(f"SELECT * FROM user_account_relation uar INNER JOIN account a ON uar.account_id=a.id WHERE uar.user_id=1 AND a.account_name='{conv_currnecy}'")
							query.set_qtype("one")
							chkacc_2=query.get_query()
							
							if(chkacc_2==None):
								query.set_query(f"INSERT INTO account SET account_name='{conv_currnecy}'")
								conv_account_id=query.insert()
								query.set_query(f"INSERT INTO user_account_relation SET user_id=1,account_id={conv_account_id},amount={amt_conv}")
								add_relation=query.insert()
							else:
								conv_account_id=chkacc_2['account_id']
								conv_newmoney=float(chkacc_2['amount'])+amt_conv
								query.set_query(f"UPDATE user_account_relation SET amount='{conv_newmoney}' WHERE user_id=1 AND account_id={conv_account_id}")
								update=query.update()

							query.set_query(f"INSERT INTO transactions SET user_id=1,account_id={account_id},amt={money},action='withdraw',conv_status='true',conv_to_account_id='{conv_account_id}',rate='{rateconv}',created_at='{datetime.datetime.now()}'")
							add_transection=query.insert()

							query.set_query(f"INSERT INTO transactions SET user_id=1,account_id={conv_account_id},amt={amt_conv},action='deposit',conv_status='true',created_at='{datetime.datetime.now()}'")
							add_transection=query.insert()
							print('Convert currency Complete\n')
							return 1

						else:
							return 0
	except Exception as e:
		print(e)

def views_wallet():
	query=connect.Connect(f"SELECT a.account_name,amount FROM user_account_relation uar INNER JOIN account a ON uar.account_id=a.id WHERE uar.user_id=1","all")
	wallet=query.get_query()
	table_format="{:<15} {:<15}"
	print(table_format.format('Your Currency','Amount'))
	for data in wallet:
		print(table_format.format(data['account_name'],data['amount']))

def views_transaction():
	query=connect.Connect(f"SELECT a.account_name,t.amt,t.action,t.conv_status,t.conv_to_account_id ,a2.account_name as account_conv,DATE_FORMAT(t.created_at,'%d/%m/%Y %H:%i:%s') as trans_date FROM transactions t INNER JOIN account a ON t.account_id=a.id LEFT JOIN account a2 ON t.conv_to_account_id=a2.id WHERE t.user_id=1 ORDER BY created_at DESC;","all")
	transactions=query.get_query()
	table_format="{:<15} {:<15} {:<15} {:<15} {:<15}"
	print(table_format.format('Currency','Action','Amount','Convert to','Action at'))
	for data in transactions:
		account_conv='-'
		if(data['account_conv']!=None):
			account_conv=data['account_conv']

		print(table_format.format(data['account_name'],data['action'],data['amt'],account_conv,data['trans_date']))
	

def convert_money(curmoney,conv_rate):
	convert_money=float(curmoney)*float(conv_rate)

	return convert_money

def currency_exchange():
	currency=input('Enter Currency to Views Exchange Rates (Example : sgd,usd,thb,...) : ').lower()
	getapi=api.API(currency)
	jsondata=getapi.get_currency()
	if 'error' in jsondata:
			print(jsondata['error'])
	else:
		currency_format="{:<15} {:<15}"

		print(currency_format.format("Currency","Exchange Rate"))
		for data in jsondata['rates']:
			print(currency_format.format(data,jsondata['rates'][data]))

def main():
	dashboard()

main()
