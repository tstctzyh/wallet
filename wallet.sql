/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : wallet

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-02-16 19:42:04
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `account`
-- ----------------------------
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of account
-- ----------------------------
INSERT INTO `account` VALUES ('26', 'thb');
INSERT INTO `account` VALUES ('27', 'jpy');

-- ----------------------------
-- Table structure for `transactions`
-- ----------------------------
DROP TABLE IF EXISTS `transactions`;
CREATE TABLE `transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `action` enum('withdraw','deposit') DEFAULT NULL,
  `amt` float(10,4) DEFAULT NULL,
  `conv_status` enum('true','false') DEFAULT 'false',
  `conv_to_account_id` int(11) DEFAULT NULL,
  `rate` float(10,4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_id` (`user_id`),
  KEY `fk_account_id` (`account_id`),
  CONSTRAINT `fk_account_id` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of transactions
-- ----------------------------
INSERT INTO `transactions` VALUES ('1', '1', '26', 'deposit', '10000.0000', 'false', null, null, '2018-02-16 18:45:39');
INSERT INTO `transactions` VALUES ('2', '1', '26', 'withdraw', '1000.0000', 'false', null, null, '2018-02-16 18:45:51');
INSERT INTO `transactions` VALUES ('3', '1', '26', 'withdraw', '5000.0000', 'true', '27', null, '2018-02-16 18:47:22');
INSERT INTO `transactions` VALUES ('4', '1', '27', 'deposit', '17009.5000', 'true', null, null, '2018-02-16 18:47:22');
INSERT INTO `transactions` VALUES ('5', '1', '27', 'deposit', '10000.0000', 'false', null, null, '2018-02-16 19:08:01');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'Supakorn', 'Chanrattanakul');

-- ----------------------------
-- Table structure for `user_account_relation`
-- ----------------------------
DROP TABLE IF EXISTS `user_account_relation`;
CREATE TABLE `user_account_relation` (
  `user_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `amount` float(10,4) DEFAULT NULL,
  PRIMARY KEY (`user_id`,`account_id`),
  KEY `fk_account` (`account_id`),
  CONSTRAINT `fk_account` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_userid` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_account_relation
-- ----------------------------
INSERT INTO `user_account_relation` VALUES ('1', '26', '4000.0000');
INSERT INTO `user_account_relation` VALUES ('1', '27', '27009.5000');
